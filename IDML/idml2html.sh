
#!/bin/bash
for f in `find -name "*.idml"`; do
java -verbose -Xmx500m -Xss1024k -jar saxon105/saxon-he-10.5.jar "$f" -xsl:AnalyseIDML.xslt -o:"$f"_idml.html 2> "$f"_idml.log 
done
