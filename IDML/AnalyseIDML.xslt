<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:fn="http://www.w3.org/2005/xpath-functions" xmlns:idPkg="http://ns.adobe.com/AdobeInDesign/idml/1.0/packaging" exclude-result-prefixes="fn idPkg">
	<!--XSLT écrite par Luc Audrain, Hachette Livre, janvier 2014, email : laudrain@hachette-livre.fr -->
	<xsl:output method="html" encoding="UTF-8" indent="yes" exclude-result-prefixes="xs"/>
	<xsl:template match="/" mode="STORY">
		<xsl:apply-templates select="//Story" mode="STORY"/>
	</xsl:template>
	<xsl:template match="Story" mode="STORY">
		<xsl:apply-templates select=".//ParagraphStyleRange" mode="STORY"/>
	</xsl:template>
	<xsl:template match="ParagraphStyleRange" mode="STORY">
		<div class="P{translate(substring-after(@AppliedParagraphStyle,'ParagraphStyle/'),'. :','___')}">
			<xsl:apply-templates select=".//CharacterStyleRange|.//Link" mode="STORY"/>
		</div>
	</xsl:template>
	<xsl:template match="CharacterStyleRange" mode="STORY">
		<xsl:choose>
			<xsl:when test="contains(@AppliedCharacterStyle, '[No character style]')">
				<xsl:apply-templates select=".//Content" mode="STORY"/>
			</xsl:when>
			<xsl:otherwise>
				<span class="C{translate(substring-after(@AppliedCharacterStyle,'CharacterStyle/'),'. :','___')}">
					<xsl:apply-templates select=".//Content" mode="STORY"/>
				</span>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template match="Link" mode="STORY">
		<img src="{@LinkResourceURI}" alt="image"/>
	</xsl:template>
<!--	<xsl:template match="Properties | XMLElement" mode="STORY"/>-->

	<xsl:template match="/">
	<html xmlns="http://www.w3.org/1999/xhtml">
		<head>
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
			<title>Test IDML</title>
			<style  type="text/css">
				<xsl:apply-templates select="fn:doc('Resources/Styles.xml')" mode="STYLES"/>
			</style>
		</head>
		<body>
		<xsl:for-each select="tokenize(Document/@StoryList ,'\s+')">
			<xsl:variable name="Story">
				<xsl:text>Stories/Story_</xsl:text><xsl:value-of select="."/><xsl:text>.xml</xsl:text>
			</xsl:variable>
			<div>
				<!--<xsl:value-of select="$Story"/>-->
				<xsl:if test="fn:doc-available($Story)">
					<xsl:apply-templates select="fn:doc($Story)" mode="STORY"/>
				</xsl:if>
			</div>
		</xsl:for-each>
		</body>
	</html>
	</xsl:template>
	<xsl:template match="idPkg:Styles" mode="STYLES">
		<xsl:apply-templates select=".//ParagraphStyle" mode="STYLES"/>
		<xsl:apply-templates select=".//CharacterStyle" mode="STYLES"/>
	</xsl:template>
	<xsl:template match="ParagraphStyle" mode="STYLES">
		<xsl:text>
.</xsl:text>
		<xsl:value-of select="concat('P',translate(@Name,'. :','___'))"/>
		<xsl:text>{</xsl:text>
			<xsl:if test="@FontStyle">font-style:<xsl:value-of select="@FontStyle"/>;</xsl:if>
			<xsl:if test="@PointSize">font-size:<xsl:value-of select="@PointSize"/>px;</xsl:if>
		<xsl:text>}</xsl:text>
	</xsl:template>
	<xsl:template match="CharacterStyle" mode="STYLES">
		<xsl:text>
.</xsl:text>
		<xsl:value-of select="concat('P',translate(@Name,'. :','___'))"/>
		<xsl:text>{</xsl:text>
			<xsl:if test="@FontStyle">font-style:<xsl:value-of select="@FontStyle"/>;</xsl:if>
			<xsl:if test="@PointSize">font-size:<xsl:value-of select="@PointSize"/>px;</xsl:if>
		<xsl:text>}</xsl:text>
	</xsl:template>

</xsl:stylesheet>
