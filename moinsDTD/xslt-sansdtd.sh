
#!/bin/bash
for f in `find -name "*.xml"`; do
java -verbose -Xmx500m -Xss1024k -jar saxon105/saxon-he-10.5.jar "$f" -xsl:IdentiqueMoinsDTD.xslt -o:"$f"ValidéSansDTD.xml 2> "$f"_VSD.log 
done