
#!/bin/bash
for f in `find -name "*ValidéSansDTD.xml"`; do
java -verbose -Xmx500m -Xss1024k -jar saxon105/saxon-he-10.5.jar "$f" -xsl:XSLT_LG2DTBook.xslt -o:"$f"DTB.xml 2> "$f"_dtb.log 
done
